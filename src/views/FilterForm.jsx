import * as React from "react";
import { Formik, Form, Field } from "formik";
import { MFTextField } from "../utils/MFTextField";

import Button from "@material-ui/core/Button";
import axios from "axios";

export const FilterView = (props) => {
	return (
		<Formik
			initialValues={{ filter1: "", filter2: "" }}
			onSubmit={(values, actions) => {
				console.log(values);

				props.filterSubmit(values);
			}}
		>
			{() => (
				<div className="signInContainer">
					<div className="signInContainer">
						<Form>
							<Field
								label="Material Name"
								type="text"
								name="filter1"
								component={MFTextField}
							/>
							<br />
							<Field
								label="Date"
								type="text"
								name="filter2"
								component={MFTextField}
							/>
							<br />
							<br />
							<div className="button-container">
								<Button variant="outlined" color="primary" type="submit" size="medium">
									Apply
								</Button>
							</div>
						</Form>
					</div>
				</div>
			)}
		</Formik>
	);
};
