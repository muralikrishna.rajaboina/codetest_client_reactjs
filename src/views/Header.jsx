import * as React from "react";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";

export const Header = ({ props }) => {
	let user = props.location.state.user;
	return (
		<Paper position="relative" color="default">
			<div
				style={{
					display: "flex",
					justifyContent: "flex-end"
				}}
			>
				<div style={{ margin: "10px" }}>Welcome Murali</div>
				<div className="button-container">
					<Button
						onClick={() => {
							props.history.push("/");
						}}
					>
						Logout
					</Button>
				</div>
			</div>
		</Paper>
	);
};
