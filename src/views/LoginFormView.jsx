import * as yup from "yup";
import { Formik, Form, Field } from "formik";
import { MFTextField } from "../utils/MFTextField";

import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";

import "../App.css";

const loginFormSchema = (p) =>
	yup.object({
		email: yup
			.string()
			.email()
			.required("Email required"),
		password: yup
			.string()
			.min(p)
			.required("Password required")
	});

export const respJson = (res, actions, props) => {
	if (res.ok) {
		actions.setSubmitting(false);
		props.history.push({
			pathname: `/dashboard`,
			state: { user: `${res.userName}` }
		});
		console.log(res);
		return res;
	} else {
		actions.setErrors({ password: "Email or Password Incorrect" });
		throw res;
	}
	// actions.setSubmitting(false);
	// console.log(res);
	// props.history.push({
	//   pathname: `/dashboard`,
	//   state: { user: `${res.email.split("@")[0]}` }
	// });
};

export const handleSubmit = async (values, actions, props) => {
	const res = await fetch(`http://f2badb1f7028.ngrok.io/user/signIn`, {
		body: JSON.stringify(values),
		headers: {
			Accept: "application/json",
			"Content-Type": "application/json"
		},
		method: "POST"
	});
	return respJson(res, actions, props);
};

export const LoginForm = ({ minPasswordLength = 3, props }) => (
	<Formik
		initialValues={{ email: "", password: "" }}
		onSubmit={(values, actions) => {
			handleSubmit(values, actions, props);
		}}
		validationSchema={loginFormSchema(minPasswordLength)}
	>
		{() => (
			<div className="signInContainer">
				<Paper>
					<h1>Login Form</h1>
					<div className="signInContainer">
						<Form>
							<Field label="Email" type="email" name="email" component={MFTextField} />
							<br />
							<Field
								label="Password"
								type="password"
								name="password"
								component={MFTextField}
							/>
							<br />
							<br />
							<div className="button-container">
								<Button variant="contained" color="primary" type="submit" size="medium">
									Submit
								</Button>
							</div>
						</Form>
					</div>
				</Paper>
			</div>
		)}
	</Formik>
);
