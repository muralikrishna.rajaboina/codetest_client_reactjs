import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";

const columns = [
	{ id: "id", label: "id", minWidth: 170 },
	{
		id: "material_name",
		label: "MaterialName",
		minWidth: 170,
		align: "right",
		format: (value) => value.toLocaleString("en-US")
	},
	{
		id: "weight",
		label: "Weight",
		minWidth: 170,
		align: "right",
		format: (value) => value.toLocaleString("en-US")
	},
	{
		id: "date",
		label: "Date",
		minWidth: 170,
		align: "right",
		format: (value) => value.toFixed(2)
	},
	{
		id: "time",
		label: "Time",
		minWidth: 170,
		align: "right",
		format: (value) => value.toFixed(2)
	}
];

const useStyles = makeStyles({
	root: {
		width: "100%"
	},
	container: {
		maxHeight: 440
	}
});

export const TableComponent = ({
	data,
	count,
	page,
	rowsPerPage,
	handleChangePage,
	handleChangeRowsPerPage
}) => {
	console.log(data);
	//   console.log(filterInfo);
	//   let filteredData = [];
	//   if (Object.keys(filterInfo).length > 0) {
	//     if (filterInfo["filter1"] !== "") {
	//       filteredData = data.filter(
	//         x => x["material_name"] === filterInfo["filter1"]
	//       );
	//     }
	//     if (filterInfo["filter2"] !== "") {
	//       filteredData = data.filter(x => x["date"] === filterInfo["filter2"]);
	//     }
	//   } else {
	//     filteredData = data;
	//   }
	const classes = useStyles();
	// const [page, setPage] = React.useState(0);
	// const [rowsPerPage, setRowsPerPage] = React.useState(5);
	// const handleChangePage = (event, newPage) => {
	//   setPage(newPage);
	// };

	// const handleChangeRowsPerPage = event => {
	//   setRowsPerPage(+event.target.value);
	//   setPage(0);
	// };

	return (
		<Paper className={classes.root}>
			<TableContainer className={classes.container}>
				<Table stickyHeader aria-label="sticky table">
					<TableHead>
						<TableRow>
							{columns.map((column) => (
								<TableCell
									key={column.id}
									align={column.align}
									style={{ minWidth: column.minWidth }}
								>
									{column.label}
								</TableCell>
							))}
						</TableRow>
					</TableHead>
					<TableBody>
						{data.length > 0 ? (
							data
								//.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
								.map((row) => {
									return (
										<TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
											{columns.map((column) => {
												const value = row[column.id];
												return (
													<TableCell key={column.id} align={column.align}>
														{column.format && typeof value === "number"
															? column.format(value)
															: value}
													</TableCell>
												);
											})}
										</TableRow>
									);
								})
						) : (
							<div></div>
						)}
					</TableBody>
				</Table>
			</TableContainer>
			<TablePagination
				rowsPerPageOptions={[5, 10, 25, 100]}
				component="div"
				count={count}
				rowsPerPage={rowsPerPage}
				page={page}
				onChangePage={handleChangePage}
				onChangeRowsPerPage={handleChangeRowsPerPage}
			/>
		</Paper>
	);
};
