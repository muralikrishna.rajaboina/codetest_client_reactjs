import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Chart from './Chart';
import Piechart from './Piechart';
import Linechart from './Linechart';
// import { Pie, Line } from 'react-chartjs-2';
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

export default function CenteredGrid() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container spacing={1}>
                <Grid item xs={4}>
                    <Paper className={classes.paper}><Chart /></Paper>
                </Grid>
                <Grid item xs={4}>
                    <Paper className={classes.paper}><Piechart /></Paper>
                </Grid>
                <Grid item xs={4}>
                    <Paper className={classes.paper}><Linechart /></Paper>
                </Grid>
            </Grid>
        </div>
    );
}
