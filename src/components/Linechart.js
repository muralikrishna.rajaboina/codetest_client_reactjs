import React, { Component } from 'react';
import { Bar, Line, Pie } from 'react-chartjs-2';

class Linechart extends Component {
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         chartData: {
    //             labels: ['January', 'February', 'March',
    //                 'April', 'May'],
    //             datasets: [
    //                 {
    //                     label: 'Rainfall',
    //                     fill: false,
    //                     lineTension: 0.5,
    //                     backgroundColor: 'rgba(75,192,192,1)',
    //                     borderColor: 'rgba(0,0,0,1)',
    //                     borderWidth: 2,
    //                     data: [65, 59, 80, 81, 56]
    //                 }
    //             ]
    //         }
    //     }
    // }
    constructor() {
        super();
        this.state = { chartData: {} };
    }

    async componentDidMount() {
        const response = await fetch(`http://f2badb1f7028.ngrok.io/linechart`);
        const json = await response.json();
        console.log(json.lables)
        this.setState({
            chartData: {
                labels: json.lables,
                datasets: [
                    {
                        label: 'Production',
                        fill: false,
                        lineTension: 0.5,
                        backgroundColor: 'rgba(75,192,192,1)',
                        borderColor: 'rgba(0,0,0,1)',
                        borderWidth: 2,
                        data: json.values
                    }
                ]
            }
        })
    }

    render() {
        return (
            <div className="chart" style={{ width: 300, height: 180 }}>
                <h6> Production     By Current Year</h6>
                <Line
                    data={this.state.chartData}

                />

            </div>
        )
    }
}


export default Linechart;
