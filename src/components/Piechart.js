import React, { Component } from 'react';
import { Bar, Line, Pie } from 'react-chartjs-2';

class Piechart extends Component {

    constructor() {
        super();
        this.state = { chartData: {} };
    }

    async componentDidMount() {
        const response = await fetch(`http://f2badb1f7028.ngrok.io/piechart`);
        const json = await response.json();
        console.log(json.lables)
        this.setState({
            chartData: {
                labels: json.lables,
                datasets: [
                    {
                        label: 'Production By Current Month', display: true,
                        backgroundColor: [
                            '#B21F00',
                            '#C9DE00',
                            '#2FDE00',
                            '#00A6B4',
                            '#6800B4'
                        ],
                        hoverBackgroundColor: [
                            '#501800',
                            '#4B5000',
                            '#175000',
                            '#003350',
                            '#35014F'
                        ],
                        data: json.values
                    }
                ]
            }
        })
    }
    render() {
        return (
            <div className="chart" style={{ width: 300, height: 180 }}>
                <h6>Production By Current Month </h6>
                <Pie
                    data={this.state.chartData}

                />
            </div>
        )
    }
}


export default Piechart;
