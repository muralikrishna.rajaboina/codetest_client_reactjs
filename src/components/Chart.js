import React, { Component } from 'react';
import { Bar, Line, Pie } from 'react-chartjs-2';
import axios from 'axios'
class Chart extends Component {

    constructor() {
        super();
        this.state = { chartData: {} };
    }

    async componentDidMount() {
        const response = await fetch(`http://f2badb1f7028.ngrok.io/barchart`);
        const json = await response.json();
        console.log(json.lables)
        this.setState({
            chartData: {
                labels: json.lables,
                datasets: [
                    {
                        label: "Production",
                        data: json.values,
                        backgroundColor: 'rgba(225,99,132,0.6)',
                        borderWidth: 1
                    }
                ]
            }
        })
    }

    render() {
        return (
            <div className="chart" style={{ width: 300, height: 180 }}>
                <h6>Production By Current Day</h6>
                <Bar
                    data={this.state.chartData}
                    options={{ scales: { yAxes: [{ ticks: { beginAtZero: true } }] } }}
                />
            </div>
        )
    }
}


export default Chart;
