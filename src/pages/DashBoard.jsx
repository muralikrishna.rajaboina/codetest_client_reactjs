import { TableComponent } from "../views/TableComponent";
import { Header } from "../views/Header";
import { FilterView } from "../views/FilterForm";
import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import axios from "axios";
import CenteredGrid from "../components/Grid";

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1
	},
	paper: {
		padding: theme.spacing(2),
		textAlign: "center",
		color: theme.palette.text.secondary
	}
}));

export const DashBoardPagePage = (props) => {
	const classes = useStyles();
	const [data, setData] = useState([]);
	const [count, setCount] = useState(0);
	const [filterInfo, setFilterInfo] = useState({ filter1: "", filter2: "" });

	const [page, setPage] = React.useState(0);
	const [rowsPerPage, setRowsPerPage] = React.useState(5);
	const handleChangePage = (event, newPage) => {
		setPage(+JSON.parse(newPage));
	};

	const handleChangeRowsPerPage = (event) => {
		setRowsPerPage(+event.target.value);
		setPage(0);
	};

	//const [filterData, setFilteredData] = useState(data);
	let url = `http://f2badb1f7028.ngrok.io/production?pageSize=${rowsPerPage}&pageIndex=${page}`;
	if (filterInfo["filter1"] !== "" && filterInfo["filter2"] === "") {
		url = `${url}&materialName=${filterInfo["filter1"]}`;
	} else if (filterInfo["filter1"] !== "" && filterInfo["filter2"] === "") {
		url = `${url}&date=${filterInfo["filter2"]}`;
	} else if (filterInfo["filter1"] !== "" && filterInfo["filter2"] !== "") {
		url = `${url}&materialName=${filterInfo["filter1"]}&date=${filterInfo["filter2"]}`;
	} else {
		url = url;
	}

	useEffect(() => {
		const fetchData = async () => {
			const result = await axios(url);

			setData(result.data.data);
			setCount(result.data.count);
		};

		fetchData();
	}, [filterInfo, page, rowsPerPage]);

	const filterSubmit = (newValue) => {
		console.log(newValue);
		setFilterInfo({ ...filterInfo, ...newValue });
	};

	return (
		<div className={classes.root}>
			<Grid container>
				<Grid item xs={2}>
					<Paper className={classes.paper}>
						<FilterView data={data} filterSubmit={filterSubmit} />
					</Paper>
				</Grid>

				<Grid item xs={10}>
					<Grid item xs={12}>
						<Paper className={classes.paper}>
							<Header props={props} />
						</Paper>
					</Grid>
					<Grid item xs={12}>
						<Paper className={classes.paper}>
							<CenteredGrid />
						</Paper>
					</Grid>
					<Paper className={classes.paper}>
						<TableComponent
							data={data}
							count={count}
							page={page}
							rowsPerPage={rowsPerPage}
							handleChangePage={handleChangePage}
							handleChangeRowsPerPage={handleChangeRowsPerPage}
						/>
					</Paper>
				</Grid>
			</Grid>
		</div>
	);
};
