import { LoginForm } from "../views/LoginFormView";

export const LoginPage = props => {
  return (
    <div>
      <div>
        <LoginForm props={props} />
      </div>
    </div>
  );
};
