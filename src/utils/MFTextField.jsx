import * as React from "react";
import TextField from "@material-ui/core/TextField";

export const MFTextField = ({ label, type, field, form }) => {
	const { errors, touched } = form;
	return (
		<label htmlFor={field.name}>
			<TextField
				placeholder={label}
				type={type}
				{...field}
				error={touched[field.name] && !!errors[field.name]}
				helperText={
					!!errors[field.name] && touched[field.name] && errors[field.name]
				}
			/>
		</label>
	);
};
